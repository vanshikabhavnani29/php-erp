<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add New Sales";
$sidebarSection = "transaction";
$sidebarSubSection = "sales";
Util::createCSRFToken();
$errors = "";
if (Session::hasSession('errors')) {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
$old = "";
if (Session::hasSession('old')) {
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once __DIR__ . "/../includes/head-section.php";
    ?>

    <!--PLACE TO ADD YOUR CUSTOM CSS-->
    <style>
        .email-verify {
            background: green;
            color: #FFF;
            padding: 5px 10px;
            font-size: .875rem;
            line-height: 1.5;
            border-radius: .2rem;
            vertical-align: middle;
            /* display: none!important; */
        }
        .display-none{
            display: none;
        }
        .text-field-error{
            outline: red !important;
            border-color: red !important;
            box-shadow: 0 0 0 0.2rem rgba(255, 0, 0, 0.25)!important;
            color: red;
        }
    </style>

</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Sales</h1>
                    </div>

                    <div class="row">

                        <div class="col-lg-12">

                            <!-- Basic Card Example -->
                            <div class="card shadow mb-4">
                                <!-- START OF CARD HEADER -->
                                <div class="card-header py-3 d-flex flex-row justify-content-end">
                                    <div class="mr-3">
                                        <input type="text" class="form-control" name="email" id="customer_email" placeholder="Enter Email Of Customer">
                                    </div>
                                    <div>
                                        <p class="email-verify d-none" id="email_verify_success">
                                            <i class="fas fa-check fa-sm text-white mr-1"></i> Email Verified
                                        </p>
                                        <p class="email-verify bg-danger d-none mb-0" id="email_verify_fail">
                                            <i class="fas fa-times fa-sm text-white mr-1"></i> Email Not Verified
                                        </p>
                                        <a href="<?= BASEPAGES; ?>add-customer.php" class="btn btn-sm btn-warning shadow-sm d-none" id="add_customer_btn" style="display:none"><i class="fas fa-users"></i> Add Customer</a>
                                        <button type="button" class="d-sm-inline-block btn btn-primary shadow-sm" name="check_email" id="check_email">
                                            <i class="fas fa-envelope fa-sm text-white"></i> Check Email
                                        </button>
                                    </div>
                                </div>

                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">
                                        <i class="fas fa-plus"></i> Sales
                                    </h6>
                                    <button type="button" onclick="addProduct();" class="d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
                                        <i class="fas fa-plus fa-sm text-white"></i> Add One More Product
                                    </button>
                                </div>

                                <!-- END OF CARD HEADER-->

                                <!--START OF CARD BODY -->
                                <form action="<?= BASEURL; ?>helper/routing.php" method="POST" id="add-category">
                                    <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token'); ?>">
                                    <input type="text" name="customer_id" id="customer_id">

                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div id="product_container">
                                                <!-- BEGIN: PRDOUCT CUSTOM CONTROL -->
                                                <div class="row product_row" id="element_1">
                                                    <!-- BEGIN: CATEGORY SELECT -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Category</label>
                                                            <select name="" id="category_1" class="form-control category_select">
                                                                <option disabled selected>Select Category</option>
                                                                <?php
                                                                $categories = $di->get("database")->readData("category", ["id", "name"], "deleted=0");
                                                                foreach ($categories as $category) {
                                                                    echo "<option value='{$category->id}'>{$category->name}</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- END: CATEGORY SELECT -->

                                                    <!-- BEGIN: PRODUCTS SELECT -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="">Products</label>
                                                            <select name="product_id[]" id="product_1" class="form-control product_select">
                                                                <option disabled selected>Select Product</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!-- END: PRODUCTS SELECT -->

                                                    <!-- BEGIN: SELLING PRICE -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Selling Price</label>
                                                            <input type="number" id="selling_price_1" value="0" class="form-control" disabled>
                                                        </div>
                                                    </div>
                                                    <!-- END: SELLING PRICE -->

                                                    <!-- BEGIN: QUANTITY -->
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label for="">Quantity</label>
                                                            <input type="number" min="1" name="quantity[]" id="quantity_1" class="form-control quantity_input" value="0">
                                                        </div>
                                                    </div>
                                                    <!-- END: QUANTITY -->

                                                    <!-- BEGIN: DISCOUNT -->
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label for="">Discount</label>
                                                            <input type="number" min="1" name="discount[]" id="discount_1" class="form-control discount_input" value="0">
                                                        </div>
                                                    </div>
                                                    <!-- END: DISCOUNT -->

                                                    <!-- BEGIN: FINAL RATE -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="">Final Rate</label>
                                                            <input type="text" id="final_price_1" class="form-control final_price_input" disabled value="0">
                                                        </div>
                                                    </div>
                                                    <!-- END: FINAL RATE -->

                                                    <!-- BEGIN: DELETE BUTTON -->
                                                    <div class="col-md-1">
                                                        <button onclick="deleteProduct(1)" type="button" class="btn btn-danger" style="margin-top:40%"><i class="far fa-trash-alt"></i></button>
                                                    </div>
                                                    <!-- END: DISCOUNT -->
                                                </div>
                                                <!-- END: PRODUCT CUSTOM CONTROL -->
                                            </div>
                                            <!--BEGIN: CARD FOOTER-->
                                            <div class="card-footer d-flex justify-content-between">
                                                <div>
                                                    <input type="submit" class="btn btn-primary" name="add_sales" value="Submit">
                                                </div>
                                                <div class="form-group row">
                                                    <label for="staticEmail" class="col-sm-4 col-form-label">Final Total</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" readonly class="form-control" id="finalTotal" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END: CARD FOOTER-->
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->
        <!-- Footer -->
        <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
        <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <?php
    require_once(__DIR__ . "/../includes/scroll-to-top.php");
    ?>
    <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>

    <!--PAGE LEVEL SCRIPTS-->
    <script src="<?= BASEASSETS; ?>js/pages/transactions/add-sales.js"></script>
</body>

</html>